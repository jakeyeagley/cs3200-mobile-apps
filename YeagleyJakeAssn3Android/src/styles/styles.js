import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	container: {
    	flex: 1,
    	justifyContent: 'center',
    	alignItems: 'center',
    	backgroundColor: '#F5FCFF',
  },
	welcome: {
    	fontSize: 35,
		fontFamily: 'Baskerville',
		color: 'red',
    	textAlign: 'center',
    	margin: 10,
  },
	instructions: {
	    textAlign: 'center',
    	color: '#333333',
    	marginBottom: 5,
  },
	button: {
		backgroundColor: 'black',
		borderRadius: 30,
		padding: 20,
		width:300,
		alignItems: 'center',
		margin: 10,
  },
	buttonText:{
		fontSize: 30,
		color: 'white',
  },
  
	redCell: {
		backgroundColor: 'red',
		borderRadius: 5,
		padding: 2,
		width: 60,
		height: 60,
		alignItems: 'center',
		margin: 1,
  },

	blueCell: {
		backgroundColor: 'lightblue',
		borderRadius: 5,
		padding: 2,
		width: 60,
		height: 60,
		alignItems: 'center',
		margin: 1,
  },
	
	greenCell: {
		backgroundColor: 'green',
		borderRadius: 5,
		padding: 2,
		width: 60,
		height: 60,
		alignItems: 'center',
		margin: 1,
  },
	blackCell: {
		backgroundColor: 'black',
		borderRadius: 5,
		padding: 2,
		width: 60,
		height: 60,
		alignItems: 'center',
		margin: 1,
  },
	yellowCell: {
		backgroundColor: 'yellow',
		borderRadius: 5,
		padding: 2,
		width: 60,
		height: 60,
		alignItems: 'center',
		margin: 1,
  },
	orangeCell: {
		backgroundColor: 'orange',
		borderRadius: 5,
		padding: 2,
		width: 60,
		height: 60,
		alignItems: 'center',
		margin: 1,
  },
	cell: {
		backgroundColor: 'white',
		borderRadius: 5,
		padding: 2,
		width: 60,
		height: 60,
		alignItems: 'center',
		margin: 1,
  },

	textScreen: {
		fontSize: 40,
		textAlign: 'center',
		marginBottom: 5
  }
});

