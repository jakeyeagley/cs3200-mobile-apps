import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity
} from 'react-native';

import styles from '../styles/styles';

export default class Button extends Component {
    render() {
        return (
            <TouchableOpacity
                style={styles.button}
                onPress={() => {this.props.start(1)}}
            >
                <Text style={styles.buttonText}>
                    Start
                </Text>
            </TouchableOpacity>
        );
    }
}



