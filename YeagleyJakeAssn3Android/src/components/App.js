/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  List
} from 'react-native';
import Button from './Button';
import Cell from './Cell';
import BlackCell from './BlackCell.js';
import BlueCell from './BlueCell.js';
import RedCell from './RedCell.js';
import GreenCell from './GreenCell.js';
import YellowCell from './YellowCell.js';
import OrangeCell from './OrangeCell.js';
import styles from '../styles/styles';
import Timer from './Timer.js';

export default class App extends Component<{}> {
  constructor(props) {
	super(props);

	this.state = {
		screen:0,
		score:1,
		value:0,
		time: 10
	}

  } 
	
  render(){
	
  if(this.state.screen == 0)
  	{
		return this.startScreen();
	}
  else if(this.state.screen == 1)
  	{
		return this.levelOne();
	}
  else if(this.state.screen == 2)
	{
		return this.inbetweenScreen();
	}
  else if(this.state.screen == 3)
    {
		return this.levelTwo();
	}
  else if(this.state.screen == 4)
	{
		return this.finalScreen();
	}
  }

  startScreen() {
	return (
		<View>	
			<Text style={styles.textScreen}>
				Welcome to tapit!
			</Text>
			<Text style={styles.textScreen}>
				click the blue square 10 times to pass the level!
			</Text>
			<Text style={styles.textScreen}>	
				Press the start button to begin
			</Text>
			<Button
				start={(val) => this.startPressed(1)}
			/>
		</View>
	);
  }

  levelOne() {
			
	return (
		<View style={{flex: 2, flexDirection: 'column', alignItems: 'center'}}>
			<View style={{flex: 2, flexDirection: 'row'}}>
				<Timer
					time={ 10 }
					trigger={() => this.changeScreen()}
				/>		

				<Text style={styles.textScreen}>
					score: {this.state.score}
					{ this.checkScore() }
				</Text>
			</View>
			<View>
				{ this.levelOneGrid() }
			</View>
		</View>
		);
  }

  inbetweenScreen(){
	return (
		<View>
			<Text style={styles.textScreen}>
				Good job!
			</Text>
			<Text style={styles.textScreen}>
				Your score was 
			</Text>
			<Text style={styles.textScreen}>
				{this.state.score}
			</Text>
			<Text style={styles.textScreen}>
				Level 2
			</Text>
			<Button
				start={(val) => this.startPressed(3)}
			/>
		</View>
		);
  }
	
  levelTwo() {
	return (
		<View style={{flex: 2, flexDirection: 'column', alignItems: 'center'}}>
			<View style={{flex: 2, flexDirection: 'row'}}>
				<View>
					<Timer
						time={ 10 }
						trigger={() => this.changeScreen()}
					/>
				</View>
				<Text style={styles.textScreen}>
					score: {this.state.score}
					{ this.checkScore() }
				</Text>
			</View>
			<View>
				{ this.levelTwoGrid() }
			</View>
		</View>
		);
  }

	
  finalScreen() {
	return (
		<View>
			<Text style={styles.textScreen}>
				Good job!
			</Text>
			<Text style={styles.textScreen}>
				Your score was 
			</Text>
			<Text style={styles.textScreen}>
				{this.state.score}
			</Text>
			<Text style={styles.textScreen}>
				To play again press start
			</Text>
			<Button
				start={(val) => this.startPressed(1)}
			/>
		</View>
		);
  }
  startPressed(val){
		console.log('start was pressed' + val)
		this.setState((prevState, props) => {
			return {
				screen: this.state.screen = val
			}
		});
		
		this.setState((prevState, props) => {
			return {
				score: this.state.score = 0
			}
		});
  }

  checkScore() {
		if(this.state.score == 10)
		{
			this.changeScreen();
		}
  }

  changeScreen() {
		this.setState((prevState,props) => {
			return {
				screen: this.state.screen + 1
			}
		});
  }

  correctCellPressed(val){
		console.log('correct cell was pressed')
		this.setState((prevState,props) => {
			return {
				score: this.state.score + 1
			}
		});

		if(this.state.time == 0)
		{
			this.setState((prevState,props) => {
				return {
					screen: this.state.screen + 1
				}
			});
		}
  }

  wrongCellPressed(val){
		 console.log('wrong cell was pressed')
		}

  levelOneGrid(){
	let grid = [];	
	
	var num = Math.floor(Math.random() * 35) + 1;
	var count = 35;

	for(let j = 0; j < 8; ++j)
	{
		let col = [];
		for(let i = 0; i < 5; ++i)
		{
			if(count-- == num)
			{
				col.push(
					<View key={i}
						style={{flexDirection: 'column'}}	
					>
						<BlueCell key={i}
							score={(val) => this.correctCellPressed(val)}
						/>
					</View>
				);

				num = 1000;
			}
			else
			{	
				col.push(
					<View key={i}
						style={{flexDirection: 'column'}}	
					>
						<Cell key={i}
							score={(val) => this.wrongCellPressed(val)}
						/>
					</View>
				);	
			}
		}

		grid.push(
			<View key={j} style={{flexDirection: 'row'}}>
				{ col }
			</View>	
			);	
	}

	return grid;
  }

  levelTwoGrid(){
	let grid = [];	
	
	var countDown = 35;

	var rand = Math.floor(Math.random() * 35) + 1;
	var rand1 = Math.floor(Math.random() * 35) + 1;
	var rand2 = Math.floor(Math.random() * 35) + 1;

	var rand3 = Math.floor(Math.random() * 35) + 1;
	var rand4 = Math.floor(Math.random() * 35) + 1;

	for(let j = 0; j < 8; ++j)
	{
		let col = [];
		for(let i = 0; i < 5; ++i)
		{
			if(countDown-- == rand)
			{
				col.push(
					<View key={i}
						style={{flexDirection: 'column'}}	
					>
						<BlueCell key={i}
							score={(val) => this.correctCellPressed(val)}
						/>
					</View>
				);

				num = 1000;
			}
			else if(rand1 == countDown)
			{	
				col.push(
					<View key={i}
						style={{flexDirection: 'column'}}	
					>
						<BlackCell key={i}
							score={(val) => this.wrongCellPressed(val)}
						/>
					</View>
				);	
				rand1-=9;
			}
			else if(rand2 == countDown)
			{	
				col.push(
					<View key={i}
						style={{flexDirection: 'column'}}	
					>
						<RedCell key={i}
							score={(val) => this.wrongCellPressed(val)}
						/>
					</View>
				);	
				rand2-=5;
			}
			else if(rand3 == countDown)
			{	
				col.push(
					<View key={i}
						style={{flexDirection: 'column'}}	
					>
						<GreenCell key={i}
							score={(val) => this.wrongCellPressed(val)}
						/>
					</View>
				);	
				rand3-=3;
			}
			else if(rand4 == countDown)
			{	
				col.push(
					<View key={i}
						style={{flexDirection: 'column'}}	
					>
						<OrangeCell key={i}
							score={(val) => this.wrongCellPressed(val)}
						/>
					</View>
				);	
				rand4-=1;
			}
			else
			{	
				col.push(
					<View key={i}
						style={{flexDirection: 'column'}}	
					>
						<YellowCell key={i}
							score={(val) => this.wrongCellPressed(val)}
						/>
					</View>
				);	
			}
		}

		grid.push(
			<View key={j} style={{flexDirection: 'row'}}>
				{ col }
			</View>	
			);	
	}

	return grid;
  }

}


