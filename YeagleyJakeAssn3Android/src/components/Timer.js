import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity
} from 'react-native';

import styles from '../styles/styles';

class Timer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			seconds: 0
		}
	}
	
	componentWillMount() {
		this.getStartTime(this.props.time);
	}

	componentDidMount() {
		 var interval = setInterval(() =>
			{ this.setState((prevState) => { 
				if(prevState.seconds == 0)
					{
						clearInterval(interval);
						this.props.trigger();
							
						return prevState
					}
				else
				{ 
					return {seconds: prevState.seconds - 1}
				};
			})}, 1000);
	}	

	getStartTime(time){
		this.setState({seconds: time});
	}

	render() {
		return (
			<Text style={styles.textScreen}>
				time: {this.state.seconds}	
			</Text>
		);
	}
}

export default Timer;
