import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity
} from 'react-native';

import styles from '../styles/styles';

export default class RedCell extends Component {
    render() {
        return (
            <TouchableOpacity
				style={styles.redCell}
                onPress={() => {this.props.score(1)}}
           >
            </TouchableOpacity>
        );
    }
}
