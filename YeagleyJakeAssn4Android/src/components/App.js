/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  FlatList,
  Alert,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
	constructor(props) {
		super(props);
		
		this.data = [];
		this.arrayOfExpressions = [];

		this.state = {
			value: ' ',
			count: 0
		}
	}	

  render() {

    return (	
      <View 
	  	style={{
 			flex: 3, 
		  	flexDirection: 'column', 
			alignItems: 'center',
	 	 	}}
	  >	
		<View style={{
			flex: 1,
			flexDirection: 'row',
			alignItems: 'center',
			maxHeight: 15,
			}}		
		>
			<TouchableOpacity style={styles.button}
					onPress={() => this.clear()}
				>
					<Text style={styles.buttonText}>
						clear
					</Text>
				</TouchableOpacity>
					<TouchableOpacity style={styles.button}		
						onPress={() => this.remove()}	
					>
						<Text style={styles.buttonText}>
							remove
						</Text>
				</TouchableOpacity>
				<TouchableOpacity style={styles.button}
					onPress={() => this.about()}	
				>
					<Text style={styles.buttonText}>
						about
					</Text>
				</TouchableOpacity>
				<TouchableOpacity style={styles.button}
					onPress={() => this.help()}		
				>
					<Text style={styles.buttonText}>
						help
					</Text>
				</TouchableOpacity>
			
				<TouchableOpacity style={styles.button}
					onPress={() => this.undo()}		
				>
					<Text style={styles.buttonText}>
						undo
					</Text>
				</TouchableOpacity>
		</View>	
	  	<FlatList
			data={this.data}
			renderItem={({item}) => 
				<TouchableOpacity
					onPress={this.expressionClicked({item})}
				>
					<View style={styles.intemContainer}>
						<Text style={styles.textItem}>{item}</Text>
					</View>
				</TouchableOpacity>
			}
			keyExtractor={(item, index) => item.id}
			style={{
				alignSelf: 'stretch',
		   	}}
		>	
		</FlatList>	
		<KeyboardAvoidingView>
	  		<TextInput
				style={{
					width:200,
					fontSize: 25,
    				textAlign: 'center',
    				margin: 10,					
					backgroundColor: 'lightblue',
				}}
				value={this.state.value}
				onChangeText={(val) => this.setState({value: val})}
				onSubmitEditing={() => this._addValue()}
			/>
		</KeyboardAvoidingView>
     </View>
    );
  }
	
  clear() {
		this.data = [];
		this.setState((prevState,props) => {
			return {
				count: 0
			}
		});
  }

  remove() {
		this.data.pop();
		this.setState((prevState,props) => {
			if(this.state.count > 0){
				return {
					count: this.state.count - 1
				}
			}
			else
				return {
					count: 0
				}
		});
  }

  about() {
		Alert.alert('Creator: Jake Yeagley');
  }

  help() {
		Alert.alert('Type in a mathmatical expression into the text field');
  }

  undo() {
		this.setState((prevState, props) => {
			return {
					value: this.arrayOfExpressions.pop()
			}
		});

		this.data.pop();
		this.setState((prevState,props) => {
			if(this.state.count > 0){
				return {
					count: this.state.count - 1
				}
			}
			else
				return {
					count: 0
				}
		});
  }

  expressionClicked = ({item}) => {
		console.log('error in react-native, onPress acts as if it is clicked when it is rendered')
		//console.log('item: ' + {item}); 	
		//console.log('clicked');
		//console.log(Object.keys(item));
		//console.log(item.key);
		//console.log('array: ' + this.arrayOfExpressions[item.key]);
		//Alert.alert(this.arrayOfExpressions[item.key]);	
  }

  _addValue = () => {		
		value = this.state.value;
		this.arrayOfExpressions.push(value);
		console.log('value: ' + value);

		let answer = this.parseExpression();
		
		if(this.state.value == ' clear')
		{
			this.clear();
			this.setState((prevState,props) => {
			return {
				value: ' '
			}
		});
		}
		else if(this.state.value == ' remove')
		{
			this.remove();
			this.setState((prevState,props) => {
			return {
				value: ' '
			}
		});
		}
		else if(this.state.value == ' undo')
		{
			this.undo();
		}
		else if(this.state.value == ' about')
		{
			this.about();
			this.setState((prevState,props) => {
			return {
				value: ' '
			}
		});
		}
		else if(this.state.value == ' help')
		{
			this.help();
			this.setState((prevState,props) => {
			return {
				value: ' '
			}
		});
		}		
		else if(answer != 'error')
		{
			this.data.push((
			<Text style={styles.welcome} key={this.state.count}>
				r{this.state.count}: {answer}
			</Text>
			));

			this.setState((prevState,props) => {
				return {
					count: this.state.count + 1
				}
			});	

			this.setState((prevState,props) => {
			return {
				value: ' '
			}
		});
		}		
  }
	
  	parseExpression = () => {	
		console.log('value: ' + this.state.value);
		let parsedValue = this.state.value.slice();
		console.log('parse: ' + parsedValue[1]);
		console.log('length: ' + parsedValue.length);
		console.log('[1]: ' + parsedValue[1] + ' ' + isNaN(parsedValue[1]));
		console.log('[2]: ' + parsedValue[2] + ' ' + isNaN(parsedValue[2]));
		let expression = this.state.value.slice();
		
		

		if(parsedValue.indexOf('r') != -1)
		{
			let expression = ' '
		}

		if(parsedValue.indexOf('r') != -1)
		{
			console.log('logged an r'+ parsedValue[2]);
			console.log(parsedValue.indexOf('r'));
				index = parsedValue.indexOf('r');
			console.log('index value: ' + parsedValue[index + 1]);	
			console.log('array: ' + this.arrayOfExpressions[index + 1]);
				expression = this.arrayOfExpressions[index + 1];
				parsedValue.slice(index + 1);
			console.log('expression: ' + expression);
			console.log('parsed: ' + parsedValue);

			if(parsedValue.indexOf('+') != -1)
				expression += '+';
			if(parsedValue.indexOf('-') != -1)
				expression += '-';
			if(parsedValue.indexOf('/') != -1)
				expression += '/';
			if(parsedValue.indexOf('*') != -1)
				expression += '*';

			console.log('expression: ' + expression);
		}
		let firstValue = 0;	
		for(let i = 1; i < parsedValue.length; i++)
		{
			if(parsedValue[i] == ' ' || parsedValue[i] == '+')
			{
				firstValue = parsedValue[i - 1];
			}
		}
		console.log('firstValue: ' + firstValue);	
		try{
			console.log(eval(expression));
			answer = eval(expression);
		}catch (e) {
			{
				Alert.alert(e.message);
				return 'error'
			}
		}
		if(answer == 'Infinity')
		{
			Alert.alert("can't divide by zero");
			return 'error';
		}

		return answer
	}

	_renderItem = ({item}) => {
		return (
				<Text 
					onPress={({item}) => this.expressionClicked({item})}
					style={styles.textItem}
				>
					{item}
				</Text>
			);
	}
}

const styles = StyleSheet.create({
	itemContainer: {
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignSelf: 'stretch',
	},
	textItem:{
		flex:1,
		textAlign: 'center'
	},
	buttonText:{
		textAlign: 'center'	
	},
	button:{
		backgroundColor: 'lightblue',
		width: 60,
		height: 25,
	},
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 50,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
