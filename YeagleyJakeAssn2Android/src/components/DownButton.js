import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity
} from 'react-native';

import styles from '../styles/styles';

export default class DownButton extends Component {
    render() {
        return (
            <TouchableOpacity
                style={styles.button}
                onPress={() => {this.props.down(-1)}}
			>
                <Text style={styles.buttonText}>
                    Down
                </Text>
            </TouchableOpacity>
        );
    }
}




