import React, { Component } from 'react'
import {
	Text
} from 'react-native';

import styles from '../styles/styles';

export default class Counter extends Component{
	constructor(props){	
		super(props);

		this.state = { count: 0 };
	}	

	increment(){
		this.setState({ count: this.state.count + 1});
	}
	decrement(){
		this.setState({ count: this.state.count - 1});
	}	
	render(){
		return(
			<Text style={styles.welcome}>
	  			{this.state.value}
			</Text>		
		);
	}
}	
