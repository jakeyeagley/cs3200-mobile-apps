import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity
} from 'react-native';

import styles from '../styles/styles';

export default class UpButton extends Component {
    render() {
        return (
            <TouchableOpacity
                style={styles.button}
                onPress={() => {this.props.up(1)}}
            >
                <Text style={styles.buttonText}>
                    Up
                </Text>
            </TouchableOpacity>
        );
    }
}









