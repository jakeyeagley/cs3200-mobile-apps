import React, { Component } from 'react';
import {
  Button,
  TouchableOpacity,
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import UpButton from './UpButton';
import DownButton from './DownButton';
import styles from '../styles/styles';
import Counter from './Counter';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component<{}> {
    constructor(props) {
        super(props);

        this.state = {
          value: 0
        }
		
      }

    render() {
        return (
          <View style={styles.container}>
		  	<UpButton
				up={(val) => this.upPressed(val)}
			/>
			<DownButton
				down={(val) => this.downPressed(val)}
			/>
            <Text style={styles.welcome}>
              {this.state.value}
            </Text>
          </View>
        );
      }
	
	upPressed(val){
			console.log('up was pressed ' + val)
			this.setState((prevState, props) => {
				return {
					value: this.state.value + val
				}
			}
		);
			
	}

	downPressed(val){
			console.log('down was pressed ' + val)
			this.setState((prevState, props) => {
			return {
					value: this.state.value + val
				}
			}
		);
	}
  }

